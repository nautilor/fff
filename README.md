# FFF file manager

fff is a simple file manager written in bash using the amazing tool `dialog`

to exit press `esc`

> Note since the first `esc` key detection by `dialog` is slow press is double to make it quick


## WHAT HE DOES
currently you can just navigate folder and open files with `xdg-open`

